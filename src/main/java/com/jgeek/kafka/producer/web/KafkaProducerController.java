package com.jgeek.kafka.producer.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaProducerController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    private final String TOPIC = "sample.topic";

    @GetMapping("/publish/{message}")
    public ResponseEntity<?> publish(@PathVariable("message") String message) {
        kafkaTemplate.send(TOPIC, message);
        return ResponseEntity.ok(message + " " + "published successfully");
    }
}
